/// imports ///

import {
    configureStore,
    combineReducers,
    getDefaultMiddleware
} from '@reduxjs/toolkit'

import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER
} from 'redux-persist'

import storage from 'redux-persist/lib/storage'

import {

} from './reducers'

/// root reducer ///

const persistConfig = {
    key: 'root',
    storage
}

const rootReducer = combineReducers({

})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,

    middleware: getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [
                FLUSH,
                REHYDRATE,
                PAUSE,
                PERSIST,
                PURGE,
                REGISTER
            ]
        }
    })
})

/// exports ///

export type RootState = ReturnType<typeof rootReducer>
export const persistor = persistStore(store)