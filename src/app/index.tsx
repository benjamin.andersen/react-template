/// imports ///

import {Provider} from 'react-redux'
import {PersistGate} from 'redux-persist/integration/react'

import {
    StylesProvider,
    ThemeProvider,
    Theme
} from '@material-ui/core'

import {
    store,
    persistor
} from 'app/store'

/// types ///

interface Props {
    theme : Theme
}

/// main ///

const App : React.FC<Props> = ({
    theme,
    children
}) => <StylesProvider injectFirst>
    <ThemeProvider theme = {theme}>
        <Provider store = {store}>
            <PersistGate
                loading = {null}
                persistor = {persistor}>
                {children}
            </PersistGate>
        </Provider>
    </ThemeProvider>
</StylesProvider>

/// exports ///

export default App