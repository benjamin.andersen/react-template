/// imports ///

import {Page} from 'components/layouts'

/// main ///

const Index : React.VFC = () => <Page>
    Hello World
</Page>

/// exports ///

export default Index