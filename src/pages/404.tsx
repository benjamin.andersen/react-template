/// imports ///

import {Page} from 'components/layouts'

/// main ///

const NotFound : React.VFC = () => <Page>
    We couldn't find the page you were looking for ...
</Page>

/// exports ///

export default NotFound