/// imports ///

import {Helmet} from 'react-helmet'

/// main ///

const Head : React.VFC = () =>
    <Helmet>
        <html lang = 'en'/>
        <title>React Template</title>

        <link
            rel = 'preconnect'
            href = 'https://fonts.gstatic.com'
        />

        <link
            rel = 'stylesheet'
            href = 'https://fonts.googleapis.com/css?family=Roboto'
        />

        <link
            rel = 'icon'
            type = 'image/x-icon'
            href = ''
        />

        <link
            rel = 'stylesheet'
            href = 'https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp'
        />

        <script
            src = 'https://kit.fontawesome.com/fbbe4f3e83.js'
            crossOrigin = 'anonymous'
        />
    </Helmet>

/// exports ///

export default Head