/// imports ///

import {
    createMuiTheme
} from '@material-ui/core'

import Head from './Head'
import Body from './Body'

/// mui theme ///

const theme = createMuiTheme({
})

/// main ///

export const Page : React.FC = ({
    children
}) => <>
    <Head/>

    <Body theme = {theme}>
        {children}
    </Body>
</>

/// exports ///

export default Page