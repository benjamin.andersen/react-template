/// imports ///

import {Theme} from '@material-ui/core'

import App from 'app'

/// types ///

interface Props {
    theme : Theme
}

/// main ///

const Body : React.FC<Props> = ({
    theme,
    children
}) => <App theme = {theme}>
    {children}
</App>

/// exports ///

export default Body