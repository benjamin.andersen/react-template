const path = require('path')

const resolve = (pathName) => path.resolve(__dirname, pathName)

module.exports.onCreateWebpackConfig = ({
    stage,
    actions,
    getConfig
}) => {
    actions.setWebpackConfig({
        resolve: {
            modules: ['node_modules', resolve('src')],
            alias: {}
        },

        devtool: 'eval-source-map'
    })

    if (
        (stage === 'build-javascript')
        || (stage === 'develop')
    ) {
        const config = getConfig()

        const miniCssExtractPlugin = config.plugins.find(
            plugin => (plugin.constructor.name === 'MiniCssExtractPlugin')
        )

        if (miniCssExtractPlugin)
            miniCssExtractPlugin.options.ignoreOrder = true

        actions.replaceWebpackConfig(config)
    }
}
