module.exports = {
    siteMetadata: {
        title: 'react-template',
        description: '',
        author: 'ElohmroW'
    },

    plugins: [
        'gatsby-plugin-react-helmet',
        'gatsby-plugin-material-ui',

        {
            resolve: 'gatsby-plugin-sass',

            options: {cssLoaderOptions: {
                esModule: false,
                modules: {namedExport: false}
            }}
        }, {
            resolve: 'gatsby-plugin-create-client-paths',
            options: {prefixes: []}
        }
    ]
}